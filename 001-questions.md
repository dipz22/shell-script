1. declare an array by name my_array and then keep all colors in vibgyor and then display it by echoing. now display by a for loop by sleeping 2 seconds each time after the display.
2. same thing do it with my_list in python and print the whole my_list and also the first member of the list. display with for loop in python and sleep for 2.4 seconds after displaying each color.
3. write a script that will make curl GET calls to the uri's present in extras/fb_url_list.txt and fetch output. For this the uri's need to be prefixed with https://www.facebook.com that can be passed as an argument to the script.
4. pass a list of string to a shell script and let it consume in the form of an array and then print each member of array after a gap of 2 seconds.
5. pass all the files and directories present in the current working directory, separated by space, to a shell script and lt it consume in the form of an array and then print each member of array after a gap of 2 seconds.
