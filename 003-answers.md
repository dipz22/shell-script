# :writing_hand: Answers

1.
  * `sed 's/skeleton/body/g; s/seek/sought/g' extras/url_list.txt |tee practice/url_list_edited.txt`
  * `:3,5s/^/  /g`. The second method is the visual mode. It should be done in this order: `ctrl v` then down arrow - select till line 4. Then, `shift+i`, then you are in insert mode and give 4 spaces. then escape. Result: all the 4 spaces are reflected on all lines - line no. 3 to line no. 5

2.
```
MacBook-Pro-van-Dipesh:ziggo-ng-devops dipeshmajumdar$ echo {1..10} |xargs echo
1 2 3 4 5 6 7 8 9 10
MacBook-Pro-van-Dipesh:ziggo-ng-devops dipeshmajumdar$ echo {1..10} |xargs -n1 echo
1
2
3
4
5
6
7
8
9
10
```
3.
    -  `  ls extras/url_list*txt |xargs -J {} mv {} ./author-dispatcher/`
    -
    ```
    MacBook-Pro-van-Dipesh:ses dipeshmajumdar$ ls *.json |xargs -J{} echo {}
    email_template.json myemail.json
    MacBook-Pro-van-Dipesh:ses dipeshmajumdar$ ls *.json |xargs -I{} echo {}
    email_template.json
    myemail.json
    ```
    -   `for i in *.txt; do echo $i; cp "$i" "$(basename "$i" .txt)-edited.doc";done;`
4.
```
cd practice
mkdir temp
ln -s temp temp1
ls -ltra
```

5.
```
find tmp -type f |xargs grep -i "namespace: nexus" |awk -F ':' '{print $1}' |uniq |xargs -n1 sed -i 's/namespace: nexus/namespace: tools/g'
```
6.
```
find . -type f -iname "*14.33.40.png"  -print0 |xargs -0 rm
```
7.
```
find . -type f -exec sed -i 's/nexus2/nexus3/g' {} +
```
