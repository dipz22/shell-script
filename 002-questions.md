## Shell script exercise

1. in the extras directory there should be a file with the string 'backofflimit' find the file

2.  
    - display all lines of this file
    - display all lines of same file except for lines which contain the string 'backofflimit'
    - now find out difference of output of above 2 commands.

3. 1. display all commits for a particular file inside a git repo - let that file be... 002-questions.md  
   2. cat *last argument of previous command*
   3. again type the penultimate command with help of fc

4. last command - pipe it with `wc -l` for knowing no. of commits using double exclamation

5. without up arrow how can you see the last command

6. get NUMBER OF commits of all \*.md files in current directory

7. get rid of the ugly new line of question-6...if you haven't gotten rid of already

8.   
    1. display 1 2 3 4 5 by for loop
    2. what is alternative of history command. use that to change above for loop to display 1 2 3 4 5 by c style for loop  
9.  
    1. type `git lot --oneline 002-questions.md`
    2. Now you know that the command is wrong and you will have to replace lot with log.
    3. How will you replace? Provide 3 menthods - 1st with carrot method and then 3rd should be global substitution.
    4. Illustrate global substitution with this example `cat 002-questions.md |gret string |gret particular`
### vim mode commands
10.  
  * 10a. somewhere in history the command for displaying commits for a file was done (in question 3). without typing run the command.
  * 10b. what is reverse search. suppose you are not happy with matching and wants the next matching then?
  * 10c.
    * a. in this file - extras/url_list_sort.txt the data is in this format -  `xyz pqr stu some-number`
    * b. sort by `pqr` only, then sort by `stu` and then sort by `some-Number` and then all this in reverse order as well...
    * c. only get the list `pqr` and then sort uniq
             and then get the value of number of occurences of each item highest to lowest and then get only top 2 list.
               on this long command - come somewhere in middle and delete first part,
                 recover, end last part, recover, now delete word by word backward
    * d. suppose your cursor is at a particular point touching a word... how will you got to the next matching word...forward and backward
    * e. suppose your cursor is at a particular point touching a word, example **further**... how will you got to the next word containing the word pattern - example **furthermost**...forward and backward
    * f. with / when you search a particular string - it is case sensitive... how will you search in a case insensitve way and then how will you again turn it off. Take the example of file 002-questions.md which contains some-number and some-Number
