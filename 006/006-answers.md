# :writing_hand: solution:
1. `echo $RANDOM`
2. `echo $(( $RANDOM % 10 + 1))`
3. `echo $(( $RANDOM % 100 + 1))`
4. `cp 006-questions.md practice/` and then `shuf 006-questions.md`
5. `shuf 006-answers.md -n 2`
6.  Here is the answer:
    ```
    $ shuf -i 1-10
    9
    7
    1
    6
    3
    8
    2
    5
    4
    10
    $ !! -n 1
    shuf -i 1-10 -n 1
    8

    ```
7.
    - Shuffle with repitition
    ```
    $ shuf -i 1-10 -r -n10
    1
    3
    10
    9
    6
    3
    4
    2
    8
    3
    ```
    - `for (( ; ; )) do shuf -e subscribe like share; sleep 1; done;`

8.  `openssl rand -base64 3`
9.  `openssl rand -hex 8`
10. `head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13 ; echo ''`
11. `head /dev/urandom | tr -dc A-Za-z0-9_,%,\&,$,.,- |head -c 13 ; echo ''`
