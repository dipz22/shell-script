### Indentation, TEE, sed, symbolic links and many more

1.
   * using tee and sed create an extra copy of url_list.txt in practice directory ... with skeleton replaced by body and seek replaced by sought .... the copy name should be url_list_edited.txt
   * in url-list-edited.txt do indentation of line 3 to line 5. Undo. Same indentation of line3 to line 5 should be done this time with the visual mode. After this delete the file extras/url_list_edited.txt

2. show the difference between `xargs` and `xargs -n1` and `xargs -0`

3.
    - in the extras directories make a directory url_list. in same extras directory there are many *.txt files but only select these -  url_list*txt and then move to a directory url_list with one command. now delete the url_list directory
    - Illustrate the difference between `-I` and `-J` by echo
    - All the *.txt files that are there in the present directory needs to be renamed to *edited.doc... for example abc.txt will be become abc-edited.doc

4. Inside practice directory - create a symbolic link that should point to temp folder inside practice folder

5. inside folder tmp, there are some folders and files and some files might containe *namespace: nexus* which needs to be changed to *namespace: tools* and this needs to happen in all the files in the folder tmp. What is the command to achieve this?

6. delete a file whose name has spaces...
7. Inside a folder there are many files - replace files that have string - nexus2, with string - nexus3
