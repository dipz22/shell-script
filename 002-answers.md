### Learn bash and vim tips and tricks to increase speed...

1.
```
$ find extras -type f |xargs grep -i backofflimit  
extras/url_preheat_list.txt:/entries/kubernetes/activedeadlineseconds-and-backofflimit
```
2.
    1. `cat extras/url_preheat_list.txt`
    2. `!! |grep -v backofflimit`
    3. `diff <(cat extras/url_preheat_list.txt) <(cat extras/url_preheat_list.txt |grep -v -i backofflimit)`  
    Note: `fc -l` should give the numbers against command 1 and 2 - let those numbers be 450 and 451. fc 450 451 will give vi editor to work upon those 2 commands. Edit them according to the requirement.

3.
   1. `git rev-list @ -- 002-questions.md`
   2. `cat !$`
   3. `fc -l` will give you numbered list.   Suppose it is 405th command - then type: `!405`

4.
!! |wc -l

5. Ctrl + p
another option is fc -1 #which allows you to edit it before you execute it



6.
`for file in *.txt ; do echo "$file"; git rev-list @ -- $file |wc -l ; done`

7.
```
for file in *.txt ; do echo -n "$file"; git rev-list @ -- $file |wc -l ; done
OR
for i in *.txt; do echo -n "$i ";  git rev-list @ -- $i| wc -l; done
```

8.
    1. 
    `for i in 1 2 3 4 5; do echo $i; done #OR`  
    `for i in {1..5}; do echo $i; done`  
    2. `fc -l`  with this the command can be found and if it's 109th for example then fc 109 will take you to the command in vi mode. but if you simply one to enter the vi mode of the last command you can do fc -1. And if it's the last to last (or the penultimate command), then it can be `fc -2`... so on and so forth...   
And now requirement is such that you want to edit on the basis of last two commands you can do this... `fc -2 -1`  
Now getting back to the c style for loop... here is how it will look:  
    `for ((i=0 ; i<5; i++)); do echo $i; done`

9. 
    1. `git lot --oneline 002-questions.md`
    2. 
    3. 1. `^lot^log^`
       2. `!!:s/lot/log`
       3. Now the above two does substitution for first occurence and if you want global level substitution then the command should be: `!!:gs/lot/log`  
       4. `!!:gs/gret/grep`
10.
  * 10a. !<number in history>
  * 10b. do a demo on ctr + r. for the next match do one more ctr + r.  come out of it unscathed with ctr + g. For this target the same git rev-list.....command
  * 10c.
    * a.
    * b. `sort -k2 url_list_sort.txt` `sort -k3 url_list_sort.txt` `sort -nk4 url_list_sort.txt` Reverse: `sort -nk4 -r url_list_sort.txt` 
    * c. `cat extras/url_list_sort.txt  |awk -F' ' '{print $2}' |sort |uniq -c |head -2`   
          `crtl + k, ctrl + u, ctrl + w, ctrl + y`
    * d. `*` and then `n` to hop forward and `N` to hop backward on these search items. Also note that just like * goes to the next matching word, `#` will go to the previous matching word
    * e. `g*` and then same `n` for forward hopping and `N` for backward hopping. Also note that similarly `g#` goes to the previous matching pattern word
    * f. in the same 002-questions.md file which you have copied in the practice folder - try to search a string with some-number and then with some-Number... you will see two entries for each. Now turn case sensitivity off by typing: `set ignorecase` and after now search of some-number should give four entries. Turn case sensitivity on by typing `set noignorecase` OR `set ignorecase!`    
