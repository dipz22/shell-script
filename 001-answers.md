1.
```
#!/bin/bash
declare -a my_array
my_array=(violet indigo blue yellow orange red)
echo ${my_array[@]}
for i in ${my_array[@]}; do
    echo $i
    sleep 2
    done
```

2.
```
#!/usr/bin/env python3
import time
my_list = ['violet', 'indigo','blue','green','yellow','orange','red']
print('my_list is:',my_list)
print('first member of list is ',my_list[0])
for i in my_list:
    print(i)
    time.sleep(2.4)
```

3.
```
#!/bin/bash
# exmaple usage ->
#. script dipeshmajumdar.com
declare -a filecontent
filecontent=(`cat extras/fb_url_list.txt`)
baseurl=$1
for i in "${filecontent[@]}"
do
echo "sending out curl GET calls for  $baseurl$line"
curl -v -o /dev/null https://www.$baseurl$i
echo -e "\n"
echo -e ".....starting next iteration"
echo -e "\n"
sleep 1
echo -e "............"
sleep 2 #for you to take note of the proceedings
echo -e "\n"
done
```

4.
```
#!/bin/bash
my_array=("$@")
for i in "${my_array[@]}"; do
    echo $i
    sleep 2
done
```

5.
```
#!/bin/bash
ls
printf "\nType the files present above separated by space:"
read input_string
my_array=($input_string)
for i in "${my_array[@]}"; do
    echo $i
    sleep 2
done

```
