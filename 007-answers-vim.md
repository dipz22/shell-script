1. `gg` is beginning of file and `G` is end of file in vim
2. '10 shift g' takes you to 10th line. '$' takes you to the end of line and '0' takes you to the beginning of line.
3.
     1. `h` `l` `j` for up and `k` for down. `w` for moving word by word. For coming to beginning of line use `0`
     2. `3w`
     3. `W`
     4. `b`
     5. 
          * `B`
          * `fq`
     6. `dw` {remember that dt<any_character> will delete the word till that character. `u` for undo. `dW` for deleting word characterized by space. `u` for undo. `Control + r` for redo.
     7. 
         1. `d$` and 2nd way is capital D
         2. `ciw` and `caw` 
         3. `diw` and `daw`
         4. `x`
         5. `r`
         6. `c$`
         7. `d$`
         8. `J`
    8. 
         1. `d0` delete to come to beginning of line now.
         2. Place the cursor on 10th line and type `O` (Capital O) for inserting a line above 10th line. Place the cursor on the character of 10th line from where you want to copy and then type `y$` and then go back to 9th line and keep cursor at position from where you want to paste the yanked portion and then type `p`  
         3. use small o
         4. get into the insert mode with samll a
    9. for deleting strings till space use `dt<space>`. Also tryp `df<space>`
    10. for deleteing strings till a particular character use `dt<character>`
    11. use a DOT for repeating same command in the next line

