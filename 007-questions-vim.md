### vim mode commands
1. copy 002-questions.md to the practice directory. Enter practice directory. vim 002-questions.md and go to the end of file and then go to the beginning of file.
2. go to 10th line and then go to end of line and then beginning of line
3. In this same 10th line -
    1. Traverse right, left,up and down. Move word by word and then come back to the beginning of the line
    2. Move forward 3 words
    3. Move forward a word characterized by space
    4. Traverse word by word **backward**
    5. 
         * Traverse word by word backwards characterized by space
         * Traverse till a particular character - `q`
    6. 
         - Delete word when the cursor is at the middle of the word and undo. 
         - Delete word characterized by sapce when the cursor is at the middle of the word and undo. Show redo functionality.
 
    7. 
         1. delete to end of line. Show there are 2 ways. undo
         2. change a particular word in and around 
         3. delete a particular word in and around
         4. delete a particular character
         5. replace a particular character
         6. go to any line, place cursor somewhere in middle. From this position delete to the end of line and remain in insert mode
         7. go to any line, place cursor somewhere in middle. From this position delete to the end of line and you should NOT remain in insert mode
         8. Go to line no. 8. Join line 8 with line 9.
   
    8. 
         1. delete to beginning of line. undo. 
         2. insert a line above the 10th line and number it as 2.5. and then...
            -  copy the entire line starting from display in the 10th line and put it in the 9th line after 2.5. Then undo. 
            -  Now cut the entire line starting from display in the 10th line and put in in the 9th line after 2.5. Then undo 
         3. insert a line below the current line and write something.
         4. esc and go to the beginning of line and then to the end of line and start typing someting after the last character. For this show how you are getting into the insert mode.
    9. delete string till space.
    10. delete string till a particular character
    11. come to the next line and repeat the same command with a shortcut
